#ifndef OTHERQTHREAD_H
#define OTHERQTHREAD_H
#include <qthread.h>

class otherQThread: public  QThread
{
      Q_OBJECT //必须加，否则出现一些奇怪问题
public:
    otherQThread(QObject * parent);
    int b;
protected:
    void run();
signals:
    void sigDone();

};

#endif // OTHERQTHREAD_H
